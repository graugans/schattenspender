# Description

The intend of the `schattenspender` is to control my shutter with the help of an raspberry PI. The Raspberry PI controls various GPIO lines which are connected to relays. There might also GPIO lines to manual overwrite the shutter state.

# License

This software is licensed under the Apache v2 license. See the file [LICENSE](LICENSE.txt) for more details.